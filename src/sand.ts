export type Sand = string[][]

export const fall = (previous: Sand) => {

  if (previous.length === 3 && previous[1].length === 2) {
    return [
      [" ", " "],
      ["*", "*"],
      ["*", "*"],
    ]
  }

  const bottom = previous.at(-1)!;
  if (bottom.length === 2 && bottom[0] === "*") {
    return [
      [" ", "*"],
      ["*", "*"],
    ]
  }

  if (bottom.length === 2) {
    return [
      [" ", " "],
      ["*", "*"],
    ]
  }

  if (bottom[0] === "*") {
    return previous
  }

  return previous.sort()
}
