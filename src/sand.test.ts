import { describe, expect, it } from "vitest"
import { fall } from "./sand"

describe("Sand", () => {
  it("falls down", () => {
    const frame = [
      ["*"],
      [" "]
    ]
    expect(fall(frame)).toEqual([
      [" "],
      ["*"]
    ])
  })
  it("falls down2", () => {
    const frame = [
      ["*"],
      [" "],
      [" "],
    ]
    expect(fall(frame)).toEqual([
      [" "],
      [" "],
      ["*"]
    ])
  })
  it("falls down 3", () => {
    const frame = [
      [" "],
      ["*"],
      [" "],
      [" "],
    ]
    expect(fall(frame)).toEqual([
      [" "],
      [" "],
      [" "],
      ["*"]
    ])
  })
  it.skip('fall down 4', () => {
    const frame = [
      [" ", "*"],
      ["*", " "],
    ]
    expect(fall(frame)).toEqual([
      [" ", " "],
      ["*", "*"]
    ])
  })
  it("overlap", () => {
    const frame = [
      ["*"],
      ["*"]
    ]
    expect(fall(frame)).toEqual([
      ["*"],
      ["*"]
    ])
  })
  it("stays", () => {
    const frame = [
      [" "],
      ["*"]]
    expect(fall(frame)).toEqual([
      [" "],
      ["*"]])
  })

  it('fall on left', () => {
    const frame = [
      [" ", "*"],
      [" ", "*"],
    ];
    expect(fall(frame)).toEqual([
      [" ", " "],
      ["*", "*"],
    ])
  })

  it('fall on left 2', () => {
    const frame = [
      [" ", "*"],
      [" ", "*"],
      ["*", "*"],
    ];
    expect(fall(frame)).toEqual([
      [" ", " "],
      ["*", "*"],
      ["*", "*"],
    ])
  })

  it('stays if bottom line is full', () => {
    const frame = [
      [" ", "*"],
      ["*", "*"],
    ];
    expect(fall(frame)).toEqual([
      [" ", "*"],
      ["*", "*"],
    ])
  })
})
